//
//  DismissVC.swift
//  test_uidynamics
//
//  Created by Harold Fong on 30/06/15.
//  Copyright (c) 2015 Harold Fong. All rights reserved.
//

import UIKit

class DismissVC: UIViewController {

    var animator: UIDynamicAnimator?
    
    @IBOutlet weak var bar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
//        animator = UIDynamicAnimator(referenceView: self.view)
//        let gravity = UIGravityBehavior(items: [self.view])
//        gravity.gravityDirection = CGVectorMake(0.0, 1)
//        animator?.addBehavior(gravity)
//        
//        let collision = UICollisionBehavior(items: [self.view])
//        animator?.addBehavior(collision)
    }
    

    @IBAction func upGesture(sender: AnyObject) {
        let gesture = sender as! UIPanGestureRecognizer

        dismissViewControllerAnimated(true, completion: nil)
        
//        if (gesture.state == UIGestureRecognizerState.Changed) {
//            let y = gesture.locationInView(self.view).y
//            self.view.frame.origin.y =  -(UIScreen.mainScreen().bounds.height - y)
//            self.view.setNeedsLayout()
//            self.view.layoutIfNeeded()
//        }
    }
}
