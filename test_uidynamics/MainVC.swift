//
//  MainVC.swift
//  test_uidynamics
//
//  Created by Harold Fong on 29/06/15.
//  Copyright (c) 2015 Harold Fong. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    // - MARK: Properties
    // Example 1
    var animator: UIDynamicAnimator?
    var pictosArray : [UIImageView]!
    
    // Example 2
    var animatorOrange : UIDynamicAnimator?
    
    // Example 3
    var animatorGreen : UIDynamicAnimator?
    
    // - MARK: Outlets
    // Example 1
    @IBOutlet weak var roundPicto1: UIImageView!
    @IBOutlet weak var roundPicto2: UIImageView!
    @IBOutlet weak var roundPicto3: UIImageView!
    @IBOutlet weak var roundPicto4: UIImageView!
    @IBOutlet weak var roundPicto5: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    
    // Example 2
    @IBOutlet weak var orangeBack: UIView!
    @IBOutlet weak var roundPicto6: UIImageView!
    @IBOutlet weak var roundPicto7: UIImageView!
    @IBOutlet weak var roundPicto8: UIImageView!
    
    // Example 3
    @IBOutlet weak var greenBack: UIView!
    @IBOutlet weak var roundPicto9: UIImageView!
    @IBOutlet weak var roundPicto10: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        
        pictosArray = [roundPicto1, roundPicto2, roundPicto3, roundPicto4, roundPicto5]
    }
    
    //MARK: - Example 1
    @IBAction func startAnimBlue(sender: AnyObject) {
        
        if (roundPicto1.frame == roundPicto2.frame) { //Open
            
            animator = UIDynamicAnimator(referenceView: self.backgroundView)
            
            // Add gravity with a vector for all [items]
            let gravity = UIGravityBehavior(items: pictosArray)
            gravity.gravityDirection = CGVectorMake(0.75, 0.0)
            animator?.addBehavior(gravity)
            
            //Space between each picto
            let space : CGFloat = 50
            //Original position of pictos
            let originX : CGFloat = roundPicto1.frame.origin.x + roundPicto1.frame.width
            
            for var i = 0; i < pictosArray.count; i++ {
                let picto = pictosArray[i]
                
                //Add a push effect to the current picto
                let push : UIPushBehavior = UIPushBehavior(items: [picto], mode: UIPushBehaviorMode.Instantaneous)
                //push with magnitude and angle (in radian)
                //here to right direction
                let magnitude : CGFloat = CGFloat(i) * 0.05
                push.setAngle(0, magnitude: magnitude)
                animator?.addBehavior(push)
                
                //Add collision for the current picto
                let collision = UICollisionBehavior(items: [picto])
                collision.translatesReferenceBoundsIntoBoundary = true
                
                //Add a "invisible" ground from point x1 to point x2
                /* Here it is like this :
            
                    x1
                    |
                    |
                    |
                    x2
                */
                let x : CGFloat = originX + CGFloat(i) * space
                collision.addBoundaryWithIdentifier("barrier\(i)", fromPoint: CGPointMake(x, 0), toPoint: CGPointMake(x, backgroundView.frame.height))
                animator?.addBehavior(collision)
            }
            
            // Add elasticity with the ground (we can add friction, resistance...)
            let behavior = UIDynamicItemBehavior(items: pictosArray)
            behavior.elasticity = 0.3
            animator?.addBehavior(behavior)
            
            (sender as! UIButton).setTitle("Close", forState: .Normal)
        } else { // Close
            animator?.removeAllBehaviors()
            UIView.animateWithDuration(0.5, animations: {
                self.backgroundView.setNeedsLayout()
                self.backgroundView.layoutIfNeeded()
            })
            
            (sender as! UIButton).setTitle("Start", forState: .Normal)
        }
    }
    
    //MARK: - Example 2
    @IBAction func startOrangeAnim(sender: AnyObject) {
        
        if (roundPicto7.frame == roundPicto8.frame) {
            
            //Add a "magnet" to a point
            animatorOrange = UIDynamicAnimator(referenceView: self.orangeBack)
            var snapBehavior1: UISnapBehavior = UISnapBehavior(item: roundPicto7, snapToPoint: CGPointMake(35, 100))
            animatorOrange?.addBehavior(snapBehavior1)
            
            var snapBehavior2: UISnapBehavior = UISnapBehavior(item: roundPicto8, snapToPoint: CGPointMake(115, 100))
            animatorOrange?.addBehavior(snapBehavior2)
            
            (sender as! UIButton).setTitle("Close", forState: .Normal)
        } else {
            animatorOrange?.removeAllBehaviors()
            UIView.animateWithDuration(0.25, animations: {
                self.orangeBack.setNeedsLayout()
                self.orangeBack.layoutIfNeeded()
            })
            
            (sender as! UIButton).setTitle("Start", forState: .Normal)
        }
    }
    
    @IBAction func startGreenAnim(sender: AnyObject) {
        animatorGreen = UIDynamicAnimator(referenceView: self.greenBack)
        
        let attachment = UIAttachmentBehavior(item: roundPicto9, attachedToAnchor: roundPicto10.center)
        animatorGreen?.addBehavior(attachment)
        
        let push : UIPushBehavior = UIPushBehavior(items: [roundPicto9], mode: UIPushBehaviorMode.Instantaneous)
        push.setAngle(0, magnitude: 1)
        animatorGreen?.addBehavior(push)
        
        let gravity = UIGravityBehavior(items: [roundPicto9])
        animatorGreen?.addBehavior(gravity)
    }
}
